<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(function () {
                    Route::prefix('businesses')->name('business.')
                        ->group(base_path('app/Domains/Business/Routes/Business.php'));
                    Route::prefix('orders')->name('order.')
                        ->group(base_path('app/Domains/Order/Routes/Business.php'))
                        ->group(base_path('app/Domains/Order/Routes/Driver.php'));
                    Route::prefix('drivers')->name('driver.')
                        ->group(base_path('app/Domains/Driver/Routes/Driver.php'));
                }

                );

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });
    }
}
