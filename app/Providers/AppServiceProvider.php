<?php

namespace App\Providers;


use App\Services\Interfaces\AuthTokenServiceInterface;
use App\Services\Interfaces\NotificationServiceInterface;
use App\Services\JwtTokenService;
use App\Services\WebHookService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(AuthTokenServiceInterface::class, JwtTokenService::class);
        $this->app->bind(NotificationServiceInterface::class, WebHookService::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
