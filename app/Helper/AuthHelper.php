<?php

namespace App\Helper;

use App\Domains\Order\Enums\AuthKey;
use Illuminate\Http\Request;

class AuthHelper
{
    static public function business_id():string
    {

        return \Illuminate\Support\Facades\Request::get("business_id");
    }

    static public function driver_id():string
    {
        return \Illuminate\Support\Facades\Request::get("driver_id");
    }
}
