<?php

namespace App\Domains\Order\Enums;

class AuthKey
{
    public const BUSINESS = 'business_id';
    public const DRIVER = 'driver_id';


    public const VALUES = [
        self::BUSINESS,
        self::DRIVER
    ];


}
