<?php

namespace App\Listeners;

use App\Domains\Business\Repositories\Interfaces\BusinessRepositoryInterface;
use App\Events\OrderUpdated;
use App\Services\Interfaces\NotificationServiceInterface;
use Illuminate\Contracts\Queue\ShouldQueue;


class WebHookCallListener implements ShouldQueue
{
    /**
     * Create the event listener.
     */
    public function __construct(
        private readonly BusinessRepositoryInterface $businessRepository,
        private readonly NotificationServiceInterface $notificationService)
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(OrderUpdated $event): void
    {
        $business = $this->businessRepository->getBusiness($event->order->business_id);
        if ($business->webhook_url!=null)
        {
            $this->notificationService->send($business->webhook_url, data: $event->order->toArray());
        }
    }
}
