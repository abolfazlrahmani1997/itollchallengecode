<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $destination_address
 * @property string $destination_latitude
 * @property string $destination_longitude
 * @property string $origin_latitude
 * @property string $origin_longitude
 * @property string $origin_address
 * @property string $business_id
 * @property string $status
 *
 */
class Order extends Model
{
    use HasFactory;
    use HasUlids;

    protected $fillable = [
        'status'
        ,'driver_id'
        , 'business_id'
        , 'origin_address'
        , 'origin_longitude'
        , 'origin_latitude'
        , 'sender_name'
        , 'destination_address'
        , 'destination_longitude'
        , 'destination_latitude'
        , 'receiver_name'
        , 'sender_mobile'
        , 'receiver_mobile'
    ];

    public function business(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Business::class, 'business_id', 'id');
    }

}
