<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUlids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $webhook_url
 */
class Business extends Model
{
    use HasFactory;
    use HasUlids;

    protected $table = 'businesses';




}
