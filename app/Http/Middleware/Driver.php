<?php

namespace App\Http\Middleware;

use App\Services\Interfaces\AuthTokenServiceInterface;
use Closure;
use Illuminate\Http\Client\HttpClientException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use function PHPUnit\Framework\isNull;

class Driver
{
    public function __construct(private readonly AuthTokenServiceInterface $authTokenService)
    {
    }

    /**
     * Handle an incoming request.
     *
     * @param \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $token = $request->header('authorization');

        throw_if(!$token, new HttpClientException('Authentication not done', Response::HTTP_FORBIDDEN));

        $token = $this->authTokenService->validateToken($token);
        if (!$token) {
            throw_if(!$token, new HttpClientException('Authentication not done', Response::HTTP_FORBIDDEN));
        }

        $token = $this->authTokenService->parserToken(str_replace('Bearer ', '', $request->header('authorization')));
        throw_if($token->claims()->get('type') != 'driver', new HttpClientException('Authentication not done', Response::HTTP_FORBIDDEN));

        $request->attributes->add(['driver_id' => $token->claims()->get('ulid')]);
        $request->setUserResolver(fn() => $token->claims()->get('ulid'));
        return $next($request);
    }
}
