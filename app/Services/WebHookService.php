<?php

namespace App\Services;

use App\Services\Interfaces\NotificationServiceInterface;
use Illuminate\Support\Facades\Http;
use SebastianBergmann\Invoker\TimeoutException;

class WebHookService implements NotificationServiceInterface
{

    public function send(string $url, array $data)
    {
        if ($url!=null)
        {

            $client = Http::accept('application/json');
            try {
                $client->post($url, ['body' => $data]); // call some resource
            } catch (TimeoutException $e) {
                sleep(5);
                $client->post($url, ['body' => $data]);
            }
        }


    }
}
