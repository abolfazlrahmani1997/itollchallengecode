<?php
namespace App\Services\Interfaces;

use Lcobucci\JWT\UnencryptedToken;

interface AuthTokenServiceInterface
{
    /**
     * Create token for the type we want
     * @param array $info
     * @return array
     */

    public function createToken(array $info): string;

    /**
     * Check if token is valid or not
     * @param string $token
     * @return bool
     */
    public function validateToken(string $token): bool;

    public function parserToken(string $jwt): UnencryptedToken;


    public function generateToken(string $name, int $id, int $exp = null): string;
}
