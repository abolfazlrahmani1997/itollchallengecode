<?php

namespace App\Services\Interfaces;

interface NotificationServiceInterface
{
    public function send(string $url,array $data);
}
