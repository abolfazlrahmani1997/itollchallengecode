<?php

namespace App\Services;



use App\Services\Interfaces\AuthTokenServiceInterface;
use DateTimeImmutable;
use JetBrains\PhpStorm\ArrayShape;
use Lcobucci\Clock\SystemClock;
use Lcobucci\JWT\Encoding\CannotDecodeContent;
use Lcobucci\JWT\Encoding\ChainedFormatter;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Token\Builder;
use Lcobucci\JWT\Token\InvalidTokenStructure;
use Lcobucci\JWT\Token\Parser;
use Lcobucci\JWT\Token\Plain;
use Lcobucci\JWT\Token\UnsupportedHeaderFound;
use Lcobucci\JWT\UnencryptedToken;
use Lcobucci\JWT\Validation\Constraint\LooseValidAt;
use Lcobucci\JWT\Validation\Validator;
use Lcobucci\JWT\Validation\Constraint\RelatedTo;

class JwtTokenService implements AuthTokenServiceInterface
{


    /**
     * Generate Token
     * @param string $uuid
     * @return Plain
     * @throws \Exception
     */
    public function createToken(array $info): string
    {
        $signingKey = InMemory::plainText("hiG8DlOKvtih6AxlZn5XKImZ06yu8I3mkOzaJrEuW8yAv8Jnkw330uMt8AEqQ5LB");
        $now = new DateTimeImmutable();
        $tokenBuilder = (new Builder(new JoseEncoder(), ChainedFormatter::default()));
        $algorithm = new Sha256();
        $token = $tokenBuilder
            ->withHeader('kid', 'sim2')
            ->issuedBy(env('APP_URL'))
            ->issuedAt($now)
            ->expiresAt($now->modify('+2 hour'))
            ->relatedTo(env('JWT'))
            ->withClaim("type", $info["type"])
            ->withClaim("ulid", $info["id"])
            ->getToken($algorithm, $signingKey)->toString();

        return $token;
    }

    /**
     * Decode Token
     * @param string $jwt
     * @return UnencryptedToken
     */
    public function parserToken(string $jwt): UnencryptedToken
    {
        $parser = new Parser(new JoseEncoder());
        try {
            $token = $parser->parse($jwt);
        } catch (CannotDecodeContent|InvalidTokenStructure|UnsupportedHeaderFound $e) {
            //todo:Set to log
            echo 'Oh no, an error: ' . $e->getMessage();
        }
        assert($token instanceof UnencryptedToken);

        return $token;
    }

    /**
     * Convert Token
     * @param Plain $token
     * @return string
     */
    public function getStringToken(Plain $token): string
    {
        return $token->toString();
    }

    /**
     * get Return uuid
     */
    public function getUuid(UnencryptedToken $token): string
    {
        return $token->claims()->get('uuid');
    }


    /**
     * Validation Token
     */
    public function validateToken(string $token): bool
    {
        $token=str_replace('Bearer ','',$token);
        $parser = new Parser(new JoseEncoder());
        $token = $parser->parse($token);

        $validator = new Validator();
        if (!$validator->validate($token, new RelatedTo(env('JWT')))) {
            return false;// will print this
        }
        return true;
    }

    /**
     * Validation Token
     */
    public function expireValidation(string $token): bool
    {
        $parser = new Parser(new JoseEncoder());
        $token = $parser->parse($token);
        $now = SystemClock::fromSystemTimezone();
        $validator = new Validator();
        if (!$validator->validate($token, new LooseValidAt($now))) {
            return false;// will print this
        }
        return true;
    }

    public function generateToken(string $name, int $id, int $exp = null): string
    {
        // TODO: Implement generateToken() method.
    }
}
