<?php

namespace App\Domains\Driver\Controller;


use App\Domains\Driver\Requests\LoginBusinessRequest;
use App\Domains\Driver\Resources\Driver;
use App\Domains\Driver\Services\Interfaces\AuthServiceInterface;
use App\Helper\ResponseWrapper;
use App\Services\Interfaces\AuthTokenServiceInterface;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    public function __construct(
        private   readonly AuthTokenServiceInterface $authTokenService
        , private readonly AuthServiceInterface $authService
        , private readonly ResponseWrapper $responseWrapper
    )
    {
    }

    public function login(LoginBusinessRequest $request)
    {

        $driver = $this->authService->login(data: $request->validated());
        $token = $this->authTokenService->createToken(info: ['type' => 'driver', 'id' => $driver->id]);;
        $driver->setAttribute('token', $token);
        return $this->responseWrapper->setResource(Driver::class)
            ->setData($driver)->generateSingleResponse();
    }

}
