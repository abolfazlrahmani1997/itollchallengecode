<?php

namespace App\Domains\Business\Controller;


use App\Domains\Business\Resources\Business;
use App\Domains\Business\Services\Interfaces\BusinessServiceInterface;
use App\Domains\Driver\Resources\Driver;
use App\Domains\Driver\Services\Interfaces\DriverServiceInterface;
use App\Helper\ResponseWrapper;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class DriverController extends Controller
{
    public function __construct(
        private readonly DriverServiceInterface $driverService,
        private readonly ResponseWrapper $responseWrapper)
    {
    }

    public function show(string $id, Request $request): \Illuminate\Http\JsonResponse
    {
        $driver = $this->driverService->get(id: $id);
        return $this->responseWrapper->setResource(resource: Driver::class)
            ->setData(data: $driver)
            ->setStatus(200)
            ->generateSingleResponse();
    }

    public function mylocation(Request $request)
    {
        $driver = $this->driverService->get();
    }



}
