<?php

namespace App\Domains\Driver\Repositories\Interfaces;

use App\Models\Driver;

interface DriverRepositoryInterface
{
    public function get(string $id = null, string $username = null): Driver;



}
