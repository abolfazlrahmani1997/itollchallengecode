<?php

namespace App\Domains\Driver\Repositories;

use App\Domains\Driver\Repositories\Interfaces\DriverRepositoryInterface;
use App\Models\Driver;
use Illuminate\Database\Eloquent\Builder;

class DriverRepository implements DriverRepositoryInterface
{
    public function get(string $id = null, string $username = null): Driver
    {
        return Driver::query()
            ->when(isset($id), fn(Builder $builder) => $builder->where('id', '=', $id))
            ->when(isset($username), fn(Builder $builder) => $builder->where('username', '=', $username))
            ->first();
    }
}
