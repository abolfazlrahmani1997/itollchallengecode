<?php

namespace App\Domains\Driver\Services;

use App\Domains\Driver\Repositories\Interfaces\DriverRepositoryInterface;
use App\Models\Driver;
use Illuminate\Database\Eloquent\Builder;

class DriverService implements DriverRepositoryInterface
{

    public function __construct(private readonly DriverRepositoryInterface $driverRepository)
    {
    }

    public function get(string $id = null, string $username = null): Driver
    {
        return $this->driverRepository->get(id: $id, username: $username);
    }
}
