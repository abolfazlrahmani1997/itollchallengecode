<?php

namespace App\Domains\Driver\Services\Interfaces;

use App\Models\Business;
use App\Models\Driver;

interface AuthServiceInterface
{

    /**
     * login Service
     * @param array $data
     * @return mixed
     */
    public function login(array $data): Driver;

}
