<?php

namespace App\Domains\Driver\Services\Interfaces;

use App\Models\Driver;

interface DriverServiceInterface
{
    public function get(string $id = null, string $username = null): Driver;


}
