<?php

namespace App\Domains\Driver\Services;

use App\Domains\Business\Repositories\Interfaces\BusinessRepositoryInterface;
use App\Domains\Driver\Repositories\Interfaces\DriverRepositoryInterface;
use App\Domains\Driver\Services\Interfaces\AuthServiceInterface;

class AuthService implements AuthServiceInterface
{

    public function __construct(
        private readonly DriverRepositoryInterface $driverRepository,
    )
    {
    }

    /**
     * login Service
     * @param array $data
     * @return mixed
     */
    public function login(array $data): \App\Models\Driver
    {
        $driver = $this->driverRepository->get(username: $data['username']);

        throw_if(!$this->validationPassword(inputPassword: $data['password']
            , password: $driver->password),);
        return $driver;
    }


    private function validationPassword(string $inputPassword, string $password): bool
    {
        return \Illuminate\Support\Facades\Hash::check($inputPassword, $password);
    }
}
