<?php

namespace App\Domains\Driver\Providers;

use App\Domains\Driver\Repositories\DriverRepository;
use App\Domains\Driver\Repositories\Interfaces\DriverRepositoryInterface;
use App\Domains\Driver\Services\AuthService;
use App\Domains\Driver\Services\DriverService;
use App\Domains\Driver\Services\Interfaces\AuthServiceInterface;
use App\Domains\Driver\Services\Interfaces\DriverServiceInterface;
use Illuminate\Support\ServiceProvider;

class DriverProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(DriverRepositoryInterface::class, DriverRepository::class);
        $this->app->bind(DriverServiceInterface::class, DriverService::class);
        $this->app->bind(AuthServiceInterface::class, AuthService::class);
    }

}
