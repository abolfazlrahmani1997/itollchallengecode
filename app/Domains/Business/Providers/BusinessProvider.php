<?php

namespace App\Domains\Business\Providers;

use App\Domains\Business\Repositories\BusinessRepository;
use App\Domains\Business\Repositories\Interfaces\BusinessRepositoryInterface;
use App\Domains\Business\Services\AuthService;
use App\Domains\Business\Services\Interfaces\AuthServiceInterface;
use Illuminate\Support\ServiceProvider;

class BusinessProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(BusinessRepositoryInterface::class, BusinessRepository::class);
        $this->app->bind(AuthServiceInterface::class, AuthService::class);
    }
}
