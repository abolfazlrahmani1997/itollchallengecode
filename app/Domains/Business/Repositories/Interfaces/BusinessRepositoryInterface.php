<?php

namespace App\Domains\Business\Repositories\Interfaces;
use App\Models\Business;
use Illuminate\Database\Eloquent\Collection;

interface BusinessRepositoryInterface
{

    /**
     * Return Business
     * @param string|null $id
     * @param string|null $userName
     * @return Business
     */
    public function getBusiness(string $id = null, string $userName = null): Business;

    /**
     * Return All Business
     * @param string|null $id
     * @param string|null $userName
     * @return Business
     */
    public function getAllBusiness(): Collection;

    /**
     * Create New Business
     * @param array $data
     * @return Business|false
     */
    public function createBusiness(array $data): Business|false;


}
