<?php

namespace App\Domains\Business\Repositories;


use App\Domains\Business\Repositories\Interfaces\BusinessRepositoryInterface;
use App\Models\Business;
use Illuminate\Database\Eloquent\Collection;
use  Illuminate\Database\Eloquent\Builder;


class BusinessRepository implements BusinessRepositoryInterface
{

    /**
     * Get Business
     * @param string|null $id
     * @param string|null $userName
     * @return Business
     */
    public function getBusiness(string $id = null, string $userName = null): \App\Models\Business
    {
        return Business::query()
            ->when(isset($id), fn(Builder $query) => $query->where('id', '=', $id))
            ->when(isset($userName), fn(Builder $query) => $query->where('username', '=', $userName))
            ->first();
    }

    /**
     * Get All Business
     * @return Collection
     */
    public function getAllBusiness(): Collection
    {
        return Business::query()->get();
    }

    /**
     * Create New Business
     * @param array $data
     * @return Business|false
     */
    public function createBusiness(array $data): \App\Models\Business|false
    {
        return Business::query()->createOrFirst($data);
    }
}
