<?php

namespace App\Domains\Business\Services;

use App\Domains\Business\Repositories\Interfaces\BusinessRepositoryInterface;
use App\Domains\Business\Services\Interfaces\AuthServiceInterface;

class AuthService implements AuthServiceInterface
{


    public function __construct(
        private readonly BusinessRepositoryInterface $businessRepository,
    )
    {
    }

    /**
     * login Service
     * @param array $data
     * @return mixed
     */
    public function login(array $data): \App\Models\Business
    {
        $business = $this->businessRepository->getBusiness(userName: $data['username']);
        throw_if(!$this->validationPassword(inputPassword: $data['password']
            , password: $business->password),);
        return $business;
    }


    private function validationPassword(string $inputPassword, string $password): bool
    {
        return \Illuminate\Support\Facades\Hash::check($inputPassword, $password);
    }
}
