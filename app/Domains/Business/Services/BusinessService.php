<?php

namespace App\Domains\Business\Services;

use App\Domains\Business\Repositories\Interfaces\BusinessRepositoryInterface;
use App\Domains\Business\Services\Interfaces\BusinessServiceInterface;
use App\Models\Business;

class BusinessService implements BusinessServiceInterface
{
    public function __construct(
        private readonly BusinessRepositoryInterface $businessRepository
    )
    {
    }

    public function getBusinessByID(string $id): Business
    {
        return $this->businessRepository->getBusiness(id: $id);
    }

    public function getBusinessByUserName(string $userName): Business
    {
        return $this->businessRepository->getBusiness(userName: $userName);
    }


}
