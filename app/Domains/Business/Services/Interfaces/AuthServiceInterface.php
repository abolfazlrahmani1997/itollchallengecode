<?php

namespace App\Domains\Business\Services\Interfaces;

use App\Models\Business;

interface AuthServiceInterface
{

    /**
     * login Service
     * @param array $data
     * @return mixed
     */
    public function login(array $data): Business;

}
