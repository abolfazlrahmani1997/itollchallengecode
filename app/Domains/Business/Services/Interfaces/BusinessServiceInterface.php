<?php

namespace App\Domains\Business\Services\Interfaces;

use App\Models\Business;
use Illuminate\Database\Eloquent\Collection;

interface BusinessServiceInterface
{

    public function getBusinessByID(string $id): Business;

    public function getBusinessByUserName(string $userName): Business;


}
