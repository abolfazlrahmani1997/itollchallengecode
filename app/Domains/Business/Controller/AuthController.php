<?php

namespace App\Domains\Business\Controller;


use App\Domains\Business\Requests\LoginBusinessRequest;
use App\Domains\Business\Resources\Business;
use App\Domains\Business\Services\Interfaces\AuthServiceInterface;
use App\Helper\ResponseWrapper;
use App\Services\Interfaces\AuthTokenServiceInterface;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class AuthController extends Controller
{
    public function __construct(
        private   readonly AuthTokenServiceInterface $authTokenService
        , private readonly AuthServiceInterface $authService
        , private readonly ResponseWrapper $responseWrapper
    )
    {
    }

    public function login(LoginBusinessRequest $request)
    {
        $business = $this->authService->login(data: $request->validated());

        $token = $this->authTokenService->createToken(info: ['type' => 'business', 'id' => $business->id]);;
        $business->setAttribute('token', $token);
        return $this->responseWrapper->setResource(Business::class)
            ->setData($business)
            ->generateSingleResponse();
    }

}
