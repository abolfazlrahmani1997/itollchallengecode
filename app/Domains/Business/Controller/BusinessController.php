<?php

namespace App\Domains\Business\Controller;


use App\Domains\Business\Resources\Business;
use App\Domains\Business\Services\Interfaces\BusinessServiceInterface;
use App\Helper\ResponseWrapper;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BusinessController extends Controller
{
    public function __construct(
        private readonly BusinessServiceInterface $businessService,
        private readonly ResponseWrapper $responseWrapper)
    {
    }

    public function show(string $id, Request $request)
    {
        $business = $this->businessService->getBusinessByID(id: $id);
        return $this->responseWrapper->setResource(resource: Business::class)
            ->setData(data: $business)
            ->setStatus(200)
            ->generateSingleResponse();
    }
}
