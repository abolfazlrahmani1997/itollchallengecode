<?php


use App\Domains\Business\Controller\AuthController;


use Illuminate\Support\Facades\Route;



    Route::prefix('auth')->name('auth.')->group(function () {
        Route::post('login', [AuthController::class, 'login'])->name('login');
    });

