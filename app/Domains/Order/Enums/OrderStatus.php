<?php
namespace App\Domains\Order\Enums;

class OrderStatus
{
    public const CREATED = 'CREATED';
    public const COMPLETED = 'COMPLETED';
    public const ACCEPTED = 'ACCEPTED';
    public const CANCELED = 'CANCELED';

    public const VALUES = [
        self::CANCELED,
        self::COMPLETED,
        self::ACCEPTED,
        self::CREATED,
    ];


}
