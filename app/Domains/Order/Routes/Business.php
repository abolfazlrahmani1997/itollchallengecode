<?php
\Illuminate\Support\Facades\Route::prefix('business')->name('business.')->middleware('business')->group(function () {
    \Illuminate\Support\Facades\Route::post('/', [\App\Domains\Order\Controllers\Business\OrderController::class, 'store'])->name('store');
    \Illuminate\Support\Facades\Route::get('/{id}', [\App\Domains\Order\Controllers\Business\OrderController::class, 'retrieve'])->name('retrieve');
    \Illuminate\Support\Facades\Route::put('/{id}', [\App\Domains\Order\Controllers\Business\OrderController::class, 'update'])->name('update');
    \Illuminate\Support\Facades\Route::get('/', [\App\Domains\Order\Controllers\Business\OrderController::class, 'get'])->name('getAll');
});
