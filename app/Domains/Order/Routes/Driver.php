<?php

\Illuminate\Support\Facades\Route::prefix('driver')->name('driver.')->middleware('driver')->group(function () {
    \Illuminate\Support\Facades\Route::get('/me', [\App\Domains\Order\Controllers\Driver\OrderController::class, 'getAll'])->name('getAll');
    \Illuminate\Support\Facades\Route::get('/{id}', [\App\Domains\Order\Controllers\Driver\OrderController::class, 'get'])->name('retrieve');
    \Illuminate\Support\Facades\Route::get('/', [\App\Domains\Order\Controllers\Driver\OrderController::class, 'getDriverOrders'])->name('history');
    \Illuminate\Support\Facades\Route::put('/{id}', [\App\Domains\Order\Controllers\Driver\OrderController::class, 'update'])->name('update');
    \Illuminate\Support\Facades\Route::post('complete/{id}', [\App\Domains\Order\Controllers\Driver\OrderController::class, 'completed'])->name('completed');
});
