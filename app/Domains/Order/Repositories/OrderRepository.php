<?php

namespace App\Domains\Order\Repositories;

use App\Domains\Order\Repositories\Interfaces\OrderRepositoryInterface;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;

class OrderRepository implements OrderRepositoryInterface
{

    public function getOrder(string $id = null, string $business_id = null, string $driver_id = null): Order
    {
        return Order::query()
            ->when(isset($id), fn(Builder $query) => $query->where('id', '=', $id))
            ->when(isset($business_id), fn(Builder $query) => $query->where('business_id', '=', $business_id))
            ->when(isset($driver_id), fn(Builder $query) => $query->where('driver_id', '=', $driver_id))
            ->firstOrFail();
    }

    public function getAllOrders(string $business_id = null, string $driver_id = null, string $status = null): Collection
    {
        return Order::query()
            ->when(isset($business_id), fn(Builder $query) => $query->where('business_id', '=', $business_id))
            ->when(isset($driver_id), fn(Builder $query) => $query->where('driver_id', '=', $driver_id))
            ->when(isset($status), fn(Builder $query) => $query->where('status', '=', $status))
            ->get();
    }

    public function createOrder(array $data): Order
    {
        return Order::query()->create($data);
    }

    public function updateOrder(Order $order, array $data): bool
    {
        return DB::transaction(fn() => $order->update($data));
    }

    public function deleteOrder(Order $order): Order
    {
        return $order->delete();
    }
}
