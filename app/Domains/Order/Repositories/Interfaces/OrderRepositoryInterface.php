<?php

namespace App\Domains\Order\Repositories\Interfaces;

use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;

interface OrderRepositoryInterface
{

    public function getOrder(string $id = null, string $business_id = null, string $driver_id = null): Order;

    public function getAllOrders(string $business_id = null, string $driver_id = null, string $status = null): Collection;

    public function createOrder(array $data): Order;

    public function updateOrder(Order $order, array $data): bool;

    public function deleteOrder(Order $order): Order;
}
