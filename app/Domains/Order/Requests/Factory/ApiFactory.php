<?php

namespace App\Domains\Order\Requests\Factory;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class ApiFactory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     * @throws HttpResponseException
     */
    protected function failedValidation(Validator $validator)
    {
        throw (new HttpResponseException(response()->json($this->formatData($validator))));
    }

    /**
     * format data
     */
    private function formatData(Validator $validator): array
    {
        return
            [
                'data' => [],
                'messages' => $validator->errors()->all()

            ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            //
        ];
    }


}
