<?php

namespace App\Domains\Order\Requests\Drivers\Orders;

use App\Domains\Order\Enums\OrderStatus;
use App\Domains\Order\Requests\Factory\ApiFactory;

class UpdateOrderRequest extends ApiFactory
{
    public function rules(): array
    {
        return [
            'status' => ['required','in:'.OrderStatus::ACCEPTED]
        ];
    }
}
