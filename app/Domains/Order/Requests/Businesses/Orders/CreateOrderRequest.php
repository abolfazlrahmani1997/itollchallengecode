<?php

namespace App\Domains\Order\Requests\Businesses\Orders;

use App\Domains\Order\Requests\Factory\ApiFactory;

class CreateOrderRequest extends ApiFactory
{
    public function rules(): array
    {
        return [
            'origin_address' => ['required']
            , 'origin_longitude' => ['required']
            , 'sender_name' => ['required']
            , 'sender_mobile' => ['required']
            , 'receiver_name' => ['required']
            , 'receiver_mobile' => ['required']
            , 'origin_latitude' => ['required']
            , 'destination_address' => ['required']
            , 'destination_longitude' => ['required']
            , 'destination_latitude' => ['required']
        ];
    }
}
