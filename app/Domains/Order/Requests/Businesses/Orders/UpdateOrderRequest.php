<?php

namespace App\Domains\Order\Requests\Businesses\Orders;

use App\Domains\Order\Enums\OrderStatus;
use App\Domains\Order\Requests\Factory\ApiFactory;

class UpdateOrderRequest extends ApiFactory
{
    public function rules(): array
    {
        return [
            'origin_address' => ['nullable']
            , 'origin_longitude' => ['nullable']
            , 'origin_latitude' => ['nullable']
            , 'destination_address' => ['nullable']
            , 'destination_longitude' => ['nullable']
            , 'destination_latitude' => ['nullable']
            , 'sender_name' => ['nullable']
            , 'receiver_name' => ['nullable']
            ,'status'=>['nullable','in:'.OrderStatus::CANCELED]

        ];
    }
}
