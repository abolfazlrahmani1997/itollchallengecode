<?php

namespace App\Domains\Order\Services\Interfaces;

use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;

interface OrderServiceInterface
{

    public function get(string $id, string $business_id = null, string $driver_id = null): Order;


    public function getAll(string $business_id = null, string $driver_id = null, string $status=null): Collection;

    public function store(array $data): Order|bool;

    public function update(string $id, array $data): bool;

    public function completed(string $id, string $driver_id): bool;


}
