<?php

namespace App\Domains\Order\Services;

use App\Domains\Order\Enums\OrderStatus;
use App\Domains\Order\Repositories\Interfaces\OrderRepositoryInterface;
use App\Domains\Order\Services\Interfaces\OrderServiceInterface;
use App\Events\OrderUpdated;
use App\Models\Order;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use function PHPUnit\Framework\isNull;

class OrderService implements OrderServiceInterface
{
    public function __construct(
        private readonly OrderRepositoryInterface $orderRepository)
    {
    }

    public function get(string $id, string $business_id = null, string $driver_id = null): Order
    {
        return $this->orderRepository->getOrder(id: $id, business_id: $business_id, driver_id: $driver_id);

    }

    public function getAll(string $business_id = null, string $driver_id = null, string $status = null): Collection
    {
        return $this->orderRepository->getAllOrders(business_id: $business_id, driver_id: $driver_id, status: $status);
    }

    public function store(array $data): Order|bool
    {
        $order = $this->orderRepository->createOrder(data: $data);
        if (!$order) {
            return false;
        }
        event(new OrderUpdated(order: $order));
        return $order;

    }

    public function update(string $id, array $data): bool
    {
        $order = $this->orderRepository->getOrder(id: $id, business_id: $data['business_id'] ?? null);
        if (!$this->checkEditAble($order)) {
            return false;
        }
        event(new OrderUpdated(order: $order));
        return Cache::lock('orders.' . $order->id)->get(function () use ($order, $data) {
            return $this->orderRepository->updateOrder(order: $order, data: $data);
        });
    }

    public function completed(string $id, string $driver_id): bool
    {
        $order = $this->orderRepository->getOrder(id: $id, driver_id: $driver_id);
        if ($order->status != OrderStatus::ACCEPTED) {
            return false;
        }
        event(new OrderUpdated(order: $order));
        return Cache::lock('orders.' . $order->id)->get(function () use ($order) {
            return $this->orderRepository->updateOrder(order: $order, data: ['status' => OrderStatus::COMPLETED]);
        });
    }

    private function checkEditAble(Order $order): bool
    {
        if (($order->status == OrderStatus::ACCEPTED || $order->status == OrderStatus::COMPLETED) && $order->driver_id != null) {
            return false;
        }
        return true;

    }
}
