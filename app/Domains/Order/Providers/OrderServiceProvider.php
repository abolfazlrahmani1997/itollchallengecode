<?php

namespace App\Domains\Order\Providers;

use App\Domains\Driver\Services\AuthService;
use App\Domains\Driver\Services\Interfaces\AuthServiceInterface;
use App\Domains\Order\Repositories\Interfaces\OrderRepositoryInterface;
use App\Domains\Order\Repositories\OrderRepository;
use App\Domains\Order\Services\Interfaces\OrderServiceInterface;
use App\Domains\Order\Services\OrderService;
use Illuminate\Support\ServiceProvider;

class OrderServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(OrderServiceInterface::class, OrderService::class);
    }

}
