<?php

namespace App\Domains\Order\Controllers\Driver;

use App\Domains\Order\Enums\OrderStatus;
use App\Domains\Order\Requests\Drivers\Orders\UpdateOrderRequest;
use App\Domains\Order\Services\Interfaces\OrderServiceInterface;
use App\Helper\AuthHelper;
use App\Helper\ResponseWrapper;
use App\Http\Resources\Order;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function __construct(
        private   readonly OrderServiceInterface $orderService
        , private readonly ResponseWrapper $responseWrapper)
    {

    }

    public function getDriverOrders(Request $request): \Illuminate\Http\JsonResponse
    {
        $orders = $this->orderService->getAll(status: OrderStatus::CREATED);
        return $this->responseWrapper
            ->setResource(Order::class)
            ->setData($orders)
            ->generateCollectionResponse();
    }

    public function getAll(Request $request): \Illuminate\Http\JsonResponse
    {

        $order = $this->orderService->getAll(driver_id: $request->get('driver_id'));
        return $this->responseWrapper
            ->setResource(Order::class)
            ->setData($order)
            ->generateCollectionResponse();
    }

    public function get(string $id, Request $request)
    {

        $order = $this->orderService->get(id: $id, driver_id: AuthHelper::driver_id());
        return $this->responseWrapper
            ->setResource(Order::class)
            ->setData($order)->generateSingleResponse();
    }

    public function update(string $id, UpdateOrderRequest $request)
    {
        $data = $request->validated();
        $data['driver_id'] = AuthHelper::driver_id();
        return $this->responseWrapper
            ->generateGeneralMessage(data: $this->orderService->update(id: $id, data: $data), code: Response::HTTP_OK);
    }

    public function completed(string $id, Request $request): \Illuminate\Http\JsonResponse
    {
        return $this->responseWrapper
            ->generateGeneralMessage($this->orderService
                ->completed(id: $id, driver_id: AuthHelper::driver_id()), code: 200);
    }


}
