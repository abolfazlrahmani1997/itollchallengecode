<?php

namespace App\Domains\Order\Controllers\Business;

use App\Domains\Order\Requests\Businesses\Orders\CreateOrderRequest;
use App\Domains\Order\Requests\Businesses\Orders\UpdateOrderRequest;
use App\Domains\Order\Services\Interfaces\OrderServiceInterface;
use App\Helper\AuthHelper;
use App\Helper\ResponseWrapper;
use App\Http\Resources\Order;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class OrderController
{
    public function __construct(
        private   readonly OrderServiceInterface $orderService
        , private readonly ResponseWrapper $responseWrapper)
    {
    }

    public function retrieve(string $id, Request $request): \Illuminate\Http\JsonResponse
    {
        $order = $this->orderService->get(id: $id, business_id: AuthHelper::business_id());
        return $this->responseWrapper
            ->setData(data: $order)
            ->setResource(Order::class)
            ->generateSingleResponse();
    }

    public function get(Request $request): \Illuminate\Http\JsonResponse
    {
        $order = $this->orderService->getAll(business_id: AuthHelper::business_id());
        return $this->responseWrapper
            ->setData(data: $order)
            ->setResource(Order::class)
            ->setStatus(Response::HTTP_OK)
            ->generateSingleResponse();
    }

    public function store(CreateOrderRequest $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->validated();
        $data['business_id'] = AuthHelper::business_id();
        $order = $this->orderService->store(data: $data);
        if (!$order) {
            return $this->responseWrapper
                ->generateFailedResponse(message: 'not create');
        }
        return $this->responseWrapper
            ->setData(data: $order)
            ->setResource(Order::class)
            ->setStatus(Response::HTTP_CREATED)
            ->generateSingleResponse();
    }

    public function update(string $id, UpdateOrderRequest $request)
    {
        $data = $request->validated();
        $data['business_id'] = AuthHelper::business_id();
        return $this->responseWrapper
            ->generateGeneralMessage(data: $this->orderService->update($id, data: $data)
                , code: Response::HTTP_OK);
    }
}
