<?php

use App\Domains\Order\Enums\OrderStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     * 'status'
     * , 'business_id'
     * , 'origin_address'
     * , 'origin_longitude'
     * , 'origin_latitude'
     * , 'destination_address'
     * , 'destination_longitude'
     * , 'destination_latitude'
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->ulid('id')->primary();
            $table->string('origin_address');
            $table->string('origin_longitude');
            $table->string('origin_latitude');
            $table->string('destination_address');
            $table->string('destination_longitude');
            $table->string('destination_latitude');
            $table->enum('status',OrderStatus::VALUES)->default(OrderStatus::CREATED);

            $table->foreignUlid('business_id')->constrained('businesses')->references('id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
